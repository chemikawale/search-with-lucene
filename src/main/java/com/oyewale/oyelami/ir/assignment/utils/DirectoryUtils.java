package com.oyewale.oyelami.ir.assignment.utils;

import com.oyewale.oyelami.ir.assignment.model.IndexDocument;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class DirectoryUtils {

    public static List<File> allFileList = new ArrayList<>();

    /**
     * Parse list of html file and return the list of their body content, title
     * and path
     *
     * @param htmlFiles
     * @return
     * @throws java.io.IOException
     */
    public static List<IndexDocument> ParseHtmlFile(List<File> htmlFiles) throws IOException {

        List<IndexDocument> parsedDocuments = new ArrayList<>();
        for (int index = 0; index < htmlFiles.size(); index++) {
            IndexDocument documentParsed = new IndexDocument();
            Document htmlDoc = Jsoup.parse(htmlFiles.get(index), "ISO-8859-1");

            documentParsed.setHtmlTitle(htmlDoc.title());
            documentParsed.setHtmlBody(htmlDoc.body());
            documentParsed.setDocumentPath(htmlFiles.get(index).getAbsolutePath());

            parsedDocuments.add(documentParsed);

        }
        return parsedDocuments;
    }

    /**
     * List all the files under a directory and sub directories
     *
     * @param directoryName to be listed
     * @return A list of Files present in a directory
     */
    public static List<File> listFiles(String directoryName) {
        File directory = new File(directoryName);

        //get all the intial files or folders from a directory
        File[] fList = directory.listFiles();

        for (File file : fList) {
            if (file.isFile()) {
                System.out.println("Now adding file: " + file.getName());
                allFileList.add(file);
            }
            if (file.isDirectory()) {
                System.out.println("--------------------------");
                System.out.println("Now processing sub directory: " + file.getName());
                System.out.println("----------------------------");

                listFiles(file.getAbsolutePath());
            }
        }

        return allFileList;
    }

}
