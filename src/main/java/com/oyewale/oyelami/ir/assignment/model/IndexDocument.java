package com.oyewale.oyelami.ir.assignment.model;

import org.jsoup.nodes.Element;

public class IndexDocument {

    private Element htmlBody;
    private String htmlTitle;
    private String documentPath;

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public Element getHtmlBody() {
        return htmlBody;
    }

    public void setHtmlBody(Element htmlBody) {
        this.htmlBody = htmlBody;
    }

    public String getHtmlTitle() {
        return htmlTitle;
    }

    public void setHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
    }
}
