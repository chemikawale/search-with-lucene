package com.oyewale.oyelami.ir.assignment;

import com.oyewale.oyelami.ir.assignment.model.IndexDocument;
import com.oyewale.oyelami.ir.assignment.utils.DirectoryUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import static org.apache.lucene.search.highlight.TokenSources.*;

/**
 *
 * @author Oyewale Oyelami
 */
public class Home {

    public static Directory directory;

    private static String pathToDocuments;
    private static String pathToIndexFolder = "default";

//    private static String pathToDocuments = "C://Users/Oyewale/Desktop/datalog";
//    private static String pathToIndexFolder = "C://Users/Oyewale/Desktop/dataindex";
    private static String rankingModel = "ok";
    private static String searchQuery = "";
    private static DirectoryReader indexReader;

    static EnglishAnalyzer analyzer = new EnglishAnalyzer();

    public static void main(String[] args) throws Exception {

        //get number of arguments supplied
        int inputSize = args.length;

        if (!(inputSize < 3 || inputSize > 4)) {

            switch (inputSize) {
                case 3:
                    pathToDocuments = args[0];
                    rankingModel = args[1];
                    searchQuery = args[2];
                    break;
                case 4:
                    pathToDocuments = args[0];
                    pathToIndexFolder = args[1];
                    rankingModel = args[2];
                    searchQuery = args[3];
                    break;

            }
            createIndex(); //create index
            searchIndex(searchQuery); //search and print query result(s)
        } else {
            System.out.println("The number of input argument is wrong \n Expecting 3 or 4 string arguments");
        }
    }

    public static void createIndex() throws IOException {

        if (pathToIndexFolder.equalsIgnoreCase("default")) {
            directory = new RAMDirectory();
        } else {
            directory = FSDirectory.open(Paths.get(pathToIndexFolder));
        }

        //check if indexes existed already in the supplied directory to avoid unnecessary duplicates
        if (!DirectoryReader.indexExists(directory)) {
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

            //seclection of preferred ranking model
            if (rankingModel.equalsIgnoreCase("vs")) {
                ClassicSimilarity vsmSimilarity = new ClassicSimilarity();
                config.setSimilarity(vsmSimilarity);

            }
            if (rankingModel.equalsIgnoreCase("ok")) {
                BM25Similarity okapiSimilarity = new BM25Similarity();
                config.setSimilarity(okapiSimilarity);

            }
            IndexWriter indexWriter = new IndexWriter(directory, config);

            List<File> listFiles = DirectoryUtils.listFiles(pathToDocuments);
            List<IndexDocument> parsedFiles = DirectoryUtils.ParseHtmlFile(listFiles);

            for (int index = 0; index < parsedFiles.size(); index++) {

                Document document = new Document();

                document.add(new Field("title", parsedFiles.get(index).getHtmlTitle(), TextField.TYPE_STORED));
                document.add(new Field("body", parsedFiles.get(index).getHtmlBody().toString(), TextField.TYPE_STORED));
                document.add(new Field("path", parsedFiles.get(index).getDocumentPath(), TextField.TYPE_STORED));

                indexWriter.addDocument(document);

                System.out.println("Indexed file: " + parsedFiles.get(index).getHtmlTitle());
            }

            indexWriter.commit();
            indexWriter.close();

            System.out.println("Files Indexing completed successfully");
            System.out.println("----------------------------\n\n");
        } else {
            System.out.println("Indexes exists already in the specified directory..");
            System.out.println("----------------------------\n\n");
        }
    }

    public static void searchIndex(String searchString) throws IOException, ParseException, InvalidTokenOffsetsException {

        indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        BooleanClause.Occur[] occurenceCondition = {BooleanClause.Occur.MUST, BooleanClause.Occur.MUST};
        Query query = MultiFieldQueryParser.parse(searchString, new String[]{"title", "body"}, occurenceCondition, analyzer);

        ScoreDoc[] hits = indexSearcher.search(query, 10).scoreDocs;

        // Iterate through the results:
        for (int i = 0; i < hits.length; i++) {

            Document hitDoc = indexSearcher.doc(hits[i].doc);

            float score = hits[i].score;
            String docTitle = hitDoc.get("title");
            String docPath = hitDoc.get("path");

            Formatter formatter = new SimpleHTMLFormatter();
            QueryScorer scorer = new QueryScorer(query);
            Highlighter highlighter = new Highlighter(formatter, scorer);

            Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, 10);

            highlighter.setTextFragmenter(fragmenter);

            TokenStream stream = getAnyTokenStream(indexReader, hits[i].doc, "body", analyzer);

            String[] highlights = highlighter.getBestFragments(stream, hitDoc.get("body"), 10);

            System.out.println("Document Rank is:" + (i + 1));
            System.out.println("Document Relevant score:" + score);
            System.out.println("Document Title:" + docTitle);
            System.out.println("Document Path:" + docPath);
            System.out.println("Query Occurrence(s) presented in Document");
            for (String highlight : highlights) {
                System.out.println("=======================");
                System.out.println(highlight);
            }

            System.out.println("--------------------------------------------------------------------------------");
        }

        indexReader.close();
        directory.close();

    }

}
